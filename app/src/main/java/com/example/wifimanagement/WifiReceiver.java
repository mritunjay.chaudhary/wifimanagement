package com.example.wifimanagement;

import Devicedb.DBHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Devicedb.Device;

import static java.lang.Integer.parseInt;

public class WifiReceiver extends BroadcastReceiver {

    Context context = MainActivity.getContext();
    DBHelper  dbHelper = new DBHelper(this.context);
    WifiManager wifiManager;
    HashMap<String, String> map = new HashMap<>();

    // Sample URL for Testing.
//    private static final String JSON_URL = "https://api.myjson.com/bins/11ztxu";
    private static final String JSON_URL = "https://api.myjson.com/bins/1anthy";
    private HashSet<String> deadap= new HashSet<>();

    // Setting wifiManager in Constructor.
    public WifiReceiver(WifiManager wifiManager) {
        this.wifiManager = wifiManager;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        }
        if(!isNetworkAvailable()){
            Log.e("Network", "Network not Available");

            // Blocking this Access point for some time.
            deadap.add(wifiManager.getConnectionInfo().getSSID().replaceAll("^\"|\"$", ""));
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    deadap.clear();
                    Log.d("clear", "deadap cleared");
                }
            }, 30000);

            wifiManager.disconnect();

            // Checking if devices table is empty. Not go in else statement if json data is already fetched.
            if(!dbHelper.CheckDeviceTableEmpty()){
                Log.d("DB", "Device Table is not Empty");

                // Getting currently available access points and devices from db.
                List<ScanResult> wifiList = wifiManager.getScanResults();
                List<Device> Devices = getAllDevices();

                for(int i = 0; i < Devices.size(); i++){
                    map.put(Devices.get(i).getSsid(), Devices.get(i).getPassword());
                }
                for(int i = 0; i < wifiList.size(); i++){
                    if(map.containsKey(wifiList.get(i).SSID) && !deadap.contains(wifiList.get(i).SSID)) {
                        Log.d("connect", "connecting to ..." + wifiList.get(i).SSID);
                        connectWiFi(wifiList.get(i), map.get(wifiList.get(i).SSID));
                        if(isNetworkAvailable()){
                            break;
                        }
                        else{
                            deadap.add(wifiManager.getConnectionInfo().getSSID().replaceAll("^\"|\"$", ""));
                            wifiManager.disconnect();
                        }
                    }
                }
            }
            else{
                Log.d("Network", "Connect internet for first time.");
                Toast.makeText(context, "Connect internet for first time.", Toast.LENGTH_SHORT).show();
            }
        }
        else{

            Log.d("Network", "Network Available");
            RequestQueue queue = Volley.newRequestQueue(context);

            // If Network is available then fetch the JSON.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, JSON_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("JSON", "json fetched");
                            JsonArray array = new JsonParser().parse(response).getAsJsonArray();
                            for(int i = 0; i < array.size(); i++){
                                JsonObject obj = array.get(i).getAsJsonObject();

                                // Deleting Previous entry and adding new entry.
                                try {
                                    dbHelper.deleteThatEntry(String.valueOf(obj.get("SSID")));
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                addDevice(String.valueOf(obj.get("SSID")), String.valueOf(obj.get("Password")), String.valueOf(obj.get("Security")), String.valueOf(obj.get("Priority")));
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("JSON", "Error in fetching the json");
                }
            });
            queue.add(stringRequest);
        }
    }

    // Adding new Device Information.
    public void addDevice(String ssid,String password, String security, String priority) {
        Device device = new Device();
//        Log.e("err", ssid);
        Log.e("err", password);
        device.setSsid(ssid.replaceAll("^\"|\"$", ""));
        device.setSecurity(security.replaceAll("^\"|\"$", ""));
        device.setPassword(password.replaceAll("^\"|\"$", ""));
        device.setPriority(parseInt(priority));
        try {
            dbHelper.createOrUpdate(device);
        }catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    // Get List of all Devices in the DB.
    public List<Device> getAllDevices() {
        List devicesList = new ArrayList<>();
        try {
            devicesList.addAll(dbHelper.getAllDevices(Device.class));
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return devicesList;
    }

    // Checking if network is available.
    public static boolean isNetworkAvailable() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e){
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        return false;
    }

    // Connect to access point function.
    private void connectWiFi(ScanResult scanResult, String password) {
        try {
            String networkSSID = scanResult.SSID;
            String networkPass = password;

            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID =  "\"" + networkSSID + "\"";
            conf.status = WifiConfiguration.Status.ENABLED;
            conf.priority = 40;

            if (scanResult.capabilities.toUpperCase().contains("WEP")) {
                Log.v("Connection", "Configuring WEP");
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

                if (networkPass.matches("^[0-9a-fA-F]+$")) {
                    conf.wepKeys[0] = networkPass;
                } else {
                    conf.wepKeys[0] = "\"".concat(networkPass).concat("\"");
                }

                conf.wepTxKeyIndex = 0;

            } else if (scanResult.capabilities.toUpperCase().contains("WPA")) {
                Log.v("Connection", "Configuring WPA");

                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

                conf.preSharedKey = "\"" + networkPass + "\"";

            } else {
                Log.e("Connection", "Can't connect to network");
            }

            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            int networkId = wifiManager.addNetwork(conf);

            Log.v("Connection", "Add result " + networkId);

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                    Log.v("Connection", "WifiConfiguration SSID " + i.SSID);

                    boolean isDisconnected = wifiManager.disconnect();
                    Log.v("Connection", "isDisconnected : " + isDisconnected);

                    boolean isEnabled = wifiManager.enableNetwork(i.networkId, true);
                    Log.v("Connection", "isEnabled : " + isEnabled);

                    boolean isReconnected = wifiManager.reconnect();
                    Log.v("Connection", "isReconnected : " + isReconnected);

                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
