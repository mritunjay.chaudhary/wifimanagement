package Devicedb;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.List;

public class DBHelper extends OrmLiteSqliteOpenHelper {
    public static final String DB_NAME = "device_manager.db";
    private static final int DB_VERSION = 1;
    ConnectionSource cs;
    SQLiteDatabase db;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {

        // Creating Table if not exist.
        this.cs = cs;
        try {
            TableUtils.createTableIfNotExists(cs, Device.class);
        } catch (SQLException | java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {
        // Doing Nothing onUgrade.
    }

    // Getting all Devices
    public List<Device> getAllDevices(Class clazz) throws SQLException, java.sql.SQLException {
        Dao<Device, Object> dao = getDao(clazz);
        return dao.queryForAll();
    }

    // Update or create entry in table.
    public Dao.CreateOrUpdateStatus createOrUpdate(Device obj) throws SQLException, java.sql.SQLException {
        Dao<Device, ?> dao = (Dao<Device, ?>) getDao(obj.getClass());
        return dao.createOrUpdate(obj);
    }

    // Deleting Entry in table Devices.
    public void deleteThatEntry(String ssid) throws java.sql.SQLException {
        Dao<Device, ?> dao = (Dao<Device, ?>) getDao(Device.class);
        DeleteBuilder<Device, ?> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq("ssid", ssid);
        deleteBuilder.delete();
    }

    // Check if devices table is empty.
    public boolean CheckDeviceTableEmpty() {
        return DatabaseUtils.queryNumEntries(db, "devices") == 0;
    }
}
